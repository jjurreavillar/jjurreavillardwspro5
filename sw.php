<?php
include_once("config.php");
require("clases.php");

$programadores = Programador::cargarDatos("programadores.txt");
$softwares = Software::cargarDatos("software.txt", $programadores);

foreach($softwares as $soft)
    if ($soft->getId() === $_GET["id"])
    {
        $software = $soft;
        break;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/general.css">
    </head>
    <body>
        <header>
            <h1><?=TITULO?></h1>
        </header>

        <h2>Datos técnicos</h2>
        <div>
            <p>ID interna: <?=$software->getId()?></p>
            <p>Nombre: <?=$software->getNombre()?></p>
            <p>Desarrolladores asignados: <?=count($software->getProgramadores())?></p>
            <br>
        </div>
        <h2>Equipo de desarrollo</h2>
        <div>
<?php
foreach ($software->getProgramadores() as $programador)
{
    echo "<p>Nombre: ".$programador->getNombre()."</p>";
    echo "<p>Identificador: ".$programador->getId()."</p>";
    foreach ($programador->getTelefonos() as $indice => $tlf)
    {
        echo "<p>";

        if ($indice == 0)
            echo "Teléfonos de contacto: ";

        echo $tlf."</p>";
    }
    echo "<br>";
}
?>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>

