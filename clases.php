<?php
class Programacion
{
    protected $id;
    protected $nombre;

    public function __construct($id, $nombre)
    {
        $this->id = $id;
        $this->nombre = $nombre;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
}

class Programador extends Programacion
{
    protected $telefonos;

    public function __construct($id, $nombre, $telefonos)
    {
        parent::__construct($id, $nombre);

        $this->telefonos = $telefonos;
    }

    public function getTelefonos()
    {
        return $this->telefonos;
    }

    public static function cargarDatos($archivo)
    {
        if (!is_file($archivo))
            return null;
        
        $programadoresArr = file($archivo);

        foreach($programadoresArr as $programador)
        {
            $p = explode(";", trim($programador));
            $programadores[] = new Programador($p[0], $p[1], explode(",", $p[2]));
        }

        return $programadores;
    }
}

class Software extends Programacion
{
    protected $programadores;

    public function __construct($id, $nombre, $programadores)
    {
        parent::__construct($id, $nombre);

        $this->programadores = $programadores;
    }

    public function agregarProgramador($programador)
    {
        $this->programadores[] = $programador;
    }

    public function getProgramadores()
    {
        return $this->programadores;
    }

    public static function cargarDatos($archivo, $programadores)
    {
        if (!is_file("software.txt") || !isset($programadores))
            return null;

        $softwaresArr = file("software.txt");

        foreach($softwaresArr as $indice => $software)
        {
            $s = explode(";", $software);

            $softwares[$indice] = new Software($s[0], $s[1], null);

            $p = explode(",", trim($s[2]));

            foreach($programadores as $prog)
                if (in_array($prog->getId(), $p))
                    $softwares[$indice]->agregarProgramador($prog);
        }

        return $softwares;
    }
}
?>

