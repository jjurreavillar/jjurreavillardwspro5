<?php
include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Proyecto</title>
        <link rel="stylesheet" href="css/general.css">
        <link rel="stylesheet" href="css/principal.css">
    </head>
    <body>
        <header>
            <h1><?=TITULO?></h1>
        </header>
        <div id="contenedor">
            <article>
                <h2>Elija una opción:</h1>
                <div id="subcontenedor">
                    <div id="programador" class="opcion"><a href="programador.php">Programador</a></div>
                    <div id="software" class="opcion"><a href="software.php">Software</a></div>
                </div>
            </article>
<?php
if (is_file("software.txt"))
{
    echo "<aside><nav><p>Software disponible:</p><ul>";
    $listaSw = file("software.txt");
    
    foreach ($listaSw as $sw)
    {
        $campos = explode(";", $sw);

        echo "<li><a href=\"sw.php?id=$campos[0]\" title=\"$campos[1]\">$campos[1]</a></li>";
    }

    echo "</ul></nav></aside>";
}
?>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>

