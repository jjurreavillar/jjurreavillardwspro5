<?php
header('Content-type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors','1');
require("clases.php");

function escribir_datos($software)
{
    $f = fopen("software.txt", "at");
    fwrite($f, $software->getId() . ';' . $software->getNombre() . ';');

    foreach ($software->getProgramadores() as $programador)
        $ids[] = $programador->getId();

    fwrite($f, implode(",", $ids) . PHP_EOL);
    fclose($f);
}

function recoge($campo)
{
    return isset($_POST[$campo]) ? htmlspecialchars(trim(strip_tags($_POST[$campo]))) : "";
}

function recoge_multiple($campo_multiple)
{
    if (isset($_POST[$campo_multiple]))
        foreach ($_POST[$campo_multiple] as $indice => $campo)
            $campo_mult[$indice] = htmlspecialchars(trim(strip_tags($campo)));

    return $campo_mult;
}

$id = recoge("id");
$nombre = recoge("nombre");
$programadorId = recoge("programador");
$progs = Programador::cargarDatos("programadores.txt");
foreach($progs as $prog)
    if($prog->getId() == $programadorId)
        $programadores[] = $prog;

$software = new Software($id, $nombre, $programadores);

escribir_datos($software);

echo "Se han escrito los datos. Se va a redirigir automáticamente a la página principal. Si no se le redirige automáticamente, <a href=\"index.php\">haga clic aquí para volver a la página principal.</a>";

header('Refresh: 5; url=index.php');
?>
